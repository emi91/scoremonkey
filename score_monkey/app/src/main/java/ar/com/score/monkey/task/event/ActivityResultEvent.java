package ar.com.score.monkey.task.event;

import android.content.Intent;

/**
 * Created by Emiliano on 3/8/15.
 */
public class ActivityResultEvent {
    public int resultCode;
    public int requestCode;
    public Intent data;

    public ActivityResultEvent(int resultCode, int requestCode, Intent data) {
        this.resultCode = resultCode;
        this.requestCode = requestCode;
        this.data = data;
    }
}
