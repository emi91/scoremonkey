package ar.com.score.monkey.utils;

/**
 * Created by Emiliano on 16/02/2015.
 */
public class ExtraConstants {
    public static final String SHARED_PREFERENCES="scoremonkey";

    public static final String CURRENT_TEMPO="currentTempo";
    public static final String CURRENT_PROGRESS="currentProgress";
    public static final String DIALOG_TITLE="Nombre";
    public static final String RECORDED_SOUND="recordedSoundExtra";
    public static final String SHEET="sheet";


}
