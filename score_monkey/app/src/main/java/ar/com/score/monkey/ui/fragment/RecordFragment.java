package ar.com.score.monkey.ui.fragment;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.SearchView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import ar.com.score.monkey.R;
import ar.com.score.monkey.client.Bus;
import ar.com.score.monkey.model.PlayerState;
import ar.com.score.monkey.model.RecordedSound;
import ar.com.score.monkey.task.SaveRecordedSoundTask;
import ar.com.score.monkey.task.event.ActivityResultEvent;
import ar.com.score.monkey.task.event.SoundRecordedEvent;
import ar.com.score.monkey.ui.activity.HomeActivity;
import ar.com.score.monkey.ui.activity.commons.BaseActivity;
import ar.com.score.monkey.ui.view.CustomAnimationDrawable;
import ar.com.score.monkey.ui.view.CustomInputDialogFragment;
import ar.com.score.monkey.ui.view.LoadingLayout;
import ar.com.score.monkey.utils.DateUtils;
import ar.com.score.monkey.utils.ExtraConstants;
import ar.com.score.monkey.utils.FileManager;
import ar.com.score.monkey.utils.ImageFilePath;
import ar.com.score.monkey.utils.SoundRecorderM4AManager;
import ar.com.score.monkey.utils.StringUtils;
import roboguice.inject.InjectView;

/**
 * Created by Emiliano on 28/04/2015.
 */
public class RecordFragment extends BaseFragment implements CustomInputDialogFragment.OnTextInputSubmitListener {
    private static final int TEMPO_MAX_VALUE=168;
    private static final int TEMPO_INIT_VALUE=40;
    private static final int TEMPO_DURATION_INIT_VALUE=60* 1000/TEMPO_INIT_VALUE;
    private static final int TEMPO_COLORED_SHOW_IMAGE_TIME=100;
    @InjectView(R.id.tempo_seekbar)
    private SeekBar tempoSeekbar;
    @InjectView(R.id.current_seekbar_value)
    private TextView currentSeekBarValue;
    @InjectView(R.id.tempo_image)
    private ImageView tempoImage;
    @InjectView(R.id.save)
    private Button saveButton;
    @Inject
    private Bus bus;
    @InjectView(R.id.loadingLayout)
    private LoadingLayout loadingLayout;
    private boolean count=false;
    private int countDown=3;


    @InjectView(R.id.minus_button)
    private View minusButton;
    @InjectView(R.id.plus_button)
    private View plusButton;
    @InjectView(R.id.record)
    private View recordButton;

    @InjectView(R.id.stop)
    private View stopButton;
    @InjectView(R.id.delete)
    private View deleteButton;
    @InjectView(R.id.play)
    private View playButton;
    @InjectView(R.id.countdown)
    private TextView countdown;


    private CustomAnimationDrawable animationDrawable;
    private ToneGenerator toneGenerator;
    private int currentTempo;
    private boolean paused=false;
    private boolean canSound=true;
    private boolean isCountingDown=false;


    //Recording
    private String audioFilename;
    private SoundRecorderM4AManager soundMp3Manager=new SoundRecorderM4AManager();
    public static int REQ_CODE_PICK_SOUNDFILE=5;


    @Override
    public void onStart() {
        super.onStart();
        bus.getBus().registerSticky(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        bus.getBus().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.record_fragment,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        currentSeekBarValue.setText(getActivity().getString(R.string.current_tempo, TEMPO_INIT_VALUE));
        loadingLayout.stopLoadingOnUIThread((BaseActivity) getActivity());
        tempoSeekbar.setMax(TEMPO_MAX_VALUE);
        toneGenerator=new ToneGenerator(AudioManager.STREAM_MUSIC,100);
        if(animationDrawable==null)
            setTemporizatorValue(TEMPO_DURATION_INIT_VALUE,TEMPO_INIT_VALUE);
        else
        {

            tempoImage.setBackground(animationDrawable);
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    animationDrawable.start();
                }
            });
        }
        tempoImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canSound = !canSound;
            }
        });

        tempoSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                currentTempo = 60 * 1000 / (seekBar.getProgress() + TEMPO_INIT_VALUE) - TEMPO_COLORED_SHOW_IMAGE_TIME;
                setTemporizatorValue(currentTempo, progress + TEMPO_INIT_VALUE);
            }


            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
        tempoSeekbar.setProgress(0);
        currentTempo=0;
        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tempoSeekbar.getProgress() < tempoSeekbar.getMax())
                    tempoSeekbar.setProgress(tempoSeekbar.getProgress() + 1);
            }
        });
        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tempoSeekbar.getProgress() > 0)
                    tempoSeekbar.setProgress(tempoSeekbar.getProgress()-1);
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(StringUtils.isEmpty(audioFilename))
                    Toast.makeText(getActivity(),"Debe primero grabar una canción.",Toast.LENGTH_SHORT).show();
                else{
                    DialogFragment dialog = CustomInputDialogFragment.newInstance(
                            getString(R.string.recorded_sound_title),
                            "Ingrese el nombre");
                    dialog.setTargetFragment(RecordFragment.this, 0);
                    dialog.show(getFragmentManager(), "");
                }
            }
        });
        initRecording();
        initPlayListener();
    }

    private void initRecording() {
        updatePlayerState(PlayerState.EMPTY);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isThereSoundRecorded()) {
                    soundMp3Manager.playSound(getActivity(), audioFilename);
                    updatePlayerState(PlayerState.PLAYING);
                } else
                    Toast.makeText(getActivity(), "No tiene sonidos cargados.", Toast.LENGTH_LONG).show();
            }
        });
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (soundMp3Manager.isPlaying()) {
                    soundMp3Manager.stopPlaying();
                    updatePlayerState(PlayerState.NOT_EMPTY);
                }
            }
        });

        recordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isCountingDown)
                    if (!soundMp3Manager.isRecording()) {
                        isCountingDown=true;
                        getView().findViewById(R.id.find).setEnabled(false);
                        saveButton.setEnabled(false);
                        minusButton.setEnabled(false);
                        plusButton.setEnabled(false);
                        recordButton.setEnabled(false);
                        countDown=((tempoSeekbar.getProgress()+TEMPO_INIT_VALUE)/40)*3;
                        animationDrawable.selectDrawable(0);
                        count=true;
                        Toast.makeText(getActivity(),"Comenzando a Grabar",Toast.LENGTH_SHORT).show();
                        animationDrawable.stop();
                        cleanSound();
                        audioFilename = getAudioFileName();
                        animationDrawable.start();
                        updatePlayerState(PlayerState.EMPTY);
                    } else {
                        stopRecordingState();
                        if(isThereSoundRecorded()) {
                            updatePlayerState(PlayerState.NOT_EMPTY);
                        }
                    }
            }
        });



        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View vew) {
                cleanSound();
                Toast.makeText(getActivity(), "Se ha borrado el sonido grabado.", Toast.LENGTH_SHORT).show();
                updatePlayerState(PlayerState.EMPTY);
            }
        });
        getView().findViewById(R.id.find).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).findAudio();
            }
        });
    }

    private void stopRecordingState() {
        if(soundMp3Manager.isRecording())
             soundMp3Manager.stopRecording();
        tempoSeekbar.setEnabled(true);
        recordButton.setActivated(false);
        countdown.setText("");
        animationDrawable.start();
        count=false;
        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getView().findViewById(R.id.find).setEnabled(true);
        saveButton.setEnabled(true);
        minusButton.setEnabled(true);
        plusButton.setEnabled(true);

    }

    private void startRecording(){
        if(!paused){
            count=false;
            countdown.setText("Grabando...");
            tempoSeekbar.setEnabled(false);
            recordButton.setEnabled(true);
            recordButton.setActivated(true);
            soundMp3Manager.startRecord(getActivity(), audioFilename);
            getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            getView().findViewById(R.id.find).setEnabled(false);
            saveButton.setEnabled(false);
            minusButton.setEnabled(false);
            plusButton.setEnabled(false);
        }
    }


    private void setTemporizatorValue(int duration,int progress) {
        currentSeekBarValue.setText(getActivity().getString(R.string.current_tempo, progress));
        animationDrawable=new CustomAnimationDrawable();
        Drawable image1 = getResources().getDrawable(R.drawable.ic_scoremonkey_grey);
        Drawable image2 = getResources().getDrawable(R.drawable.scoremonkey_orange);
        animationDrawable.addFrame(image1, duration);
        animationDrawable.addFrame(image2, TEMPO_COLORED_SHOW_IMAGE_TIME);
        animationDrawable.setOneShot(false);
        animationDrawable.setOnSecondFrame(new CustomAnimationDrawable.OnSecondFrame() {
            @Override
            public void onSecondFrame() {
                if (getActivity() != null && ((HomeActivity) getActivity()).isRecordFragmentVisible() && !canSound){
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_ABBR_ALERT, 10);
                }
                if(count){
                    countDown--;

                    if(countDown==0){
                        startRecording();
                        isCountingDown=false;
                    }
                    else
                        countdown.setText(Integer.toString(countDown) );
                }
            }
        });

        tempoImage.setBackground(animationDrawable);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                animationDrawable.start();
            }
        });
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ExtraConstants.CURRENT_TEMPO, currentTempo);
    }

    public void onEvent(ActivityResultEvent event) {
        if (event.requestCode == REQ_CODE_PICK_SOUNDFILE && event.resultCode == Activity.RESULT_OK){
            if ((event.data != null) && (event.data.getData() != null)){
                cleanSound();
                String filePath = null;
                Uri _uri = event.data.getData();
                Log.d("", "URI = " + _uri);
                filePath = ImageFilePath.getPath(getActivity(),_uri);

                audioFilename=getAudioFileName();
                File fileFrom=new File(filePath);

                try {
                    FileManager.copyFileToInternalStorage(getActivity(),fileFrom,audioFilename);
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        }
    }

    public String getAudioFileName() {
        return "music" + System.currentTimeMillis()
                + ".m4a";
    }

    public void cleanSound(){
        //TODO ver
//        if(!StringUtils.isEmpty(audioFilename))
//            FileManager.removeFileFromInternalStorage(getActivity(),audioFilename);
        audioFilename="";

    }


    public boolean isThereSoundRecorded(){
        return !StringUtils.isEmpty(audioFilename);
    }

    @Override
    public void onPause() {
        super.onPause();
        paused=true;
        stopRecordingState();
        if(soundMp3Manager.isRecording()){
            if(isThereSoundRecorded()) {
                updatePlayerState(PlayerState.NOT_EMPTY);
            }
        }
        soundMp3Manager.stopPlaying();
    }

    @Override
    public void onResume() {
        super.onResume();
        paused=false;
    }

    @Override
    public void onTextSubmit(String text) {
        RecordedSound recordedSound=new RecordedSound();
        recordedSound.setId(new Date().getTime());
        recordedSound.setName(text );
        recordedSound.setDate(DateUtils.format(new Date(), DateUtils.DDMMYYYYHHMM_DATE_FORMAT));
        recordedSound.setPathToFile(audioFilename);
        recordedSound.setTempo(Integer.toString(tempoSeekbar.getProgress()+TEMPO_INIT_VALUE));
        loadingLayout.startLoading();
        new SaveRecordedSoundTask(getActivity(),recordedSound).execute();
    }
    public void onEvent(SoundRecordedEvent event){
        loadingLayout.stopLoading();
    }

    private void initPlayListener(){
        soundMp3Manager.setCompleteListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                updatePlayerState(PlayerState.NOT_EMPTY);
            }
        });
    }

    private void updatePlayerState(PlayerState playerState){
        stopButton.setEnabled(playerState.isStopEnabled());
        playButton.setEnabled(playerState.isPlayEnabled());
        deleteButton.setEnabled(playerState.isDeleteEnabled());
    }

    public void onEvent(HomeActivity.BackEvent event){
        cleanSound();
    }
}
