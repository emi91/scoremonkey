package ar.com.score.monkey.client;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import java.io.File;

import ar.com.score.monkey.BuildConfig;
import ar.com.score.monkey.http.GenerateSheet;
import ar.com.score.monkey.http.GenerateSheetResponse;
import ar.com.score.monkey.http.ScoreMonkeyError;
import ar.com.score.monkey.task.event.OnRequestError;
import ar.com.score.monkey.utils.FileManager;
import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

/**
 * This class is the responsable to connect to the API. Here lies all the
 * connections between the client and the server.<br>
 * As <b>Jake Wharton</b> said:
 * "RestAdapter and the created instances should be treated as singletons"
 */
@Singleton
public class AndroidClient {
    @Inject
    public Bus bus;
    private static final String BASE_URL = "http://scoremonkey.herokuapp.com";

    private RestAdapter restAdapter;
    private IAndroidAuthorityClient androidAuthorityClient;


    public AndroidClient() {
//        if (restAdapter == null || androidAuthorityClient == null) {
//
//            Type typeOfFeeds = new com.google.gson.reflect.TypeToken<List<Feed>>(){}.getType();
//
//            Gson gson = new GsonBuilder().registerTypeAdapter(typeOfFeeds, new GsonDeserializer()).create();
//
            restAdapter = new RestAdapter.Builder().setErrorHandler(new ErrorHandler() {
                @Override
                public Throwable handleError(RetrofitError cause) {

                    ScoreMonkeyError error= (ScoreMonkeyError) cause.getBodyAs(ScoreMonkeyError.class);
                    bus.getBus().postSticky(new OnRequestError(error.getErrors().get(0)));
                    return null;
                }
            })
                    .setEndpoint(BASE_URL)
                    .build();
//
            if(BuildConfig.DEBUG) { // Good old trick
                restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
            }else {
                restAdapter.setLogLevel(RestAdapter.LogLevel.NONE);
            }
            androidAuthorityClient = restAdapter.create(IAndroidAuthorityClient.class);
//        }
    }

    public GenerateSheetResponse getGeneratedSheet(Context context,GenerateSheet generateSheet) {
        File file= FileManager.getFileFromInternalStorage(context,generateSheet.getFilePath());
        TypedFile typedSound = new TypedFile("application/octet-stream", file);
        return androidAuthorityClient.getGeneratedSheet(typedSound,new TypedString(generateSheet.getMinor()),new TypedString(generateSheet.getMajor()),new TypedString(generateSheet.getTempo()),new TypedString(generateSheet.getKey()),new TypedString(generateSheet.getClef()));
    }

}
