package ar.com.score.monkey.model;

import java.io.Serializable;
import java.util.Date;

import ar.com.score.monkey.utils.DateUtils;

/**
 * Created by Emiliano on 3/8/15.
 */
public class RecordedSound implements Serializable {
    private Long id;
    private String name;
    private String date;
    private String pathToFile;
    private String tempo;

    public RecordedSound() {
    }

    public RecordedSound(String name, String date, String pathToFile) {
        this.name = name;
        this.date = date;
        this.pathToFile = pathToFile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPathToFile() {
        return pathToFile;
    }

    public void setPathToFile(String pathToFile) {
        this.pathToFile = pathToFile;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTempo() {
        return tempo;
    }


    public void setTempo(String tempo) {
        this.tempo = tempo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RecordedSound that = (RecordedSound) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (pathToFile != null ? !pathToFile.equals(that.pathToFile) : that.pathToFile != null)
            return false;
        return !(tempo != null ? !tempo.equals(that.tempo) : that.tempo != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (pathToFile != null ? pathToFile.hashCode() : 0);
        result = 31 * result + (tempo != null ? tempo.hashCode() : 0);
        return result;
    }
}
