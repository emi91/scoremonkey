package ar.com.score.monkey.http;

import java.util.List;

/**
 * Created by Emiliano on 8/10/15.
 */
public class ScoreMonkeyError {
    private List<String> errors;

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
