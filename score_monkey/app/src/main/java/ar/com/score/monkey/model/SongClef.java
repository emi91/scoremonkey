package ar.com.score.monkey.model;

import ar.com.score.monkey.R;

/**
 * Created by emi91_000 on 11/05/2015.
 */
public enum SongClef {
    G(R.drawable.ic_launcher),F(R.drawable.ic_launcher),C(R.drawable.ic_launcher);
    private int resId;

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    private SongClef(int resId){
        this.resId=resId;
    }

    public static SongClef getSongClefByPosition(int position){
        for (SongClef songClef:SongClef.values()){
            if(songClef.ordinal()==position)
                return songClef;
        }
        return null;
    }
}
