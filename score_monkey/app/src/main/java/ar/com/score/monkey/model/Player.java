package ar.com.score.monkey.model;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.spoledge.aacdecoder.MultiPlayer;

/**
 * Created by Emiliano on 07/05/2015.
 */
@Singleton
public class Player {
    @Inject
    private MultiPlayer player;

    public MultiPlayer getPlayer() {
        return player;
    }

    public void setPlayer(MultiPlayer player) {
        this.player = player;
    }
}
