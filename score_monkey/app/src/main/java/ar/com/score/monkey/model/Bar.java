package ar.com.score.monkey.model;

/**
 * Created by emi91_000 on 11/05/2015.
 */
public class Bar {
    private Integer minor;
    private Integer major;

    public Integer getMinor() {
        return minor;
    }

    public void setMinor(Integer minor) {
        this.minor = minor;
    }

    public Integer getMajor() {
        return major;
    }

    public void setMajor(Integer major) {
        this.major = major;
    }
}
