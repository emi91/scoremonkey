package ar.com.score.monkey.module;

import java.util.List;

import ar.com.score.monkey.model.Sheet;
import ar.com.score.monkey.model.Song;

/**
 * Created by Emiliano on 07/05/2015.
 */
public class OnGetSheetsEvent {
    public List<Sheet> items;

    public OnGetSheetsEvent(List<Sheet> items) {
        this.items = items;
    }
}
