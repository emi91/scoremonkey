package ar.com.score.monkey.module;


import android.app.Application;

import ar.com.score.monkey.ScoreMonkeyApplication;

import com.google.inject.AbstractModule;
import com.google.inject.Inject;

public class ApplicationModuleProvider extends AbstractModule {

    private final ScoreMonkeyApplication application;

    @Inject
    public ApplicationModuleProvider(final Application application) {
        super();
        this.application = (ScoreMonkeyApplication) application;
    }

    @Override
    protected void configure() {
        bind(ScoreMonkeyApplication.class).toInstance(application);
    }
}
