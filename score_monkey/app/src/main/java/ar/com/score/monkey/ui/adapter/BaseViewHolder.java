package ar.com.score.monkey.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by ignacio on 16/04/2015.
 */
public class BaseViewHolder extends RecyclerView.ViewHolder{
    public OnViewHolderClickListener onClickListener;

    public BaseViewHolder(View itemView) {
        super(itemView);
    }

    public void setOnClickListener(final OnViewHolderClickListener onClickListener) {
        this.onClickListener = onClickListener;
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onClickListener!=null)
                    onClickListener.onViewHolderClickListener(BaseViewHolder.this);
            }
        });
    }

    public interface OnViewHolderClickListener{
        public void onViewHolderClickListener(BaseViewHolder holder);
    }
}
