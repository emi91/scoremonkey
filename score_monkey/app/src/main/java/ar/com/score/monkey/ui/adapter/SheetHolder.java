package ar.com.score.monkey.ui.adapter;

/**
 * Created by Emiliano on 28/04/2015.
 */

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import ar.com.score.monkey.R;

public class SheetHolder extends BaseViewHolder {
    TextView title;
    TextView description;

    public SheetHolder(View itemView) {
        super(itemView);
        title= (TextView) itemView.findViewById(R.id.title);
        description=(TextView)itemView.findViewById(R.id.description);
    }
}
