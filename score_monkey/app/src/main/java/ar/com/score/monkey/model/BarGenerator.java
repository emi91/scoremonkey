package ar.com.score.monkey.model;

import java.util.List;

import ar.com.score.monkey.utils.Lists;

/**
 * Created by emi91_000 on 11/05/2015.
 */
public class BarGenerator {
    public static List<String> getMajorValue(){
        List<String> minorValues=Lists.newArrayList();
        for (Integer i = 1; i <= 32; i++) {
                minorValues.add(i.toString());
        }
        return minorValues;
    }

    public static List<String> getMinorValues(){
        List<String> majorValues=Lists.newArrayList("1","2","4","8","16");
        return majorValues;
    }

}
