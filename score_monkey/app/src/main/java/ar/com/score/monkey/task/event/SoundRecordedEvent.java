package ar.com.score.monkey.task.event;

/**
 * Created by Emiliano on 4/8/15.
 */
public class SoundRecordedEvent {
    public Long id;

    public SoundRecordedEvent(Long id) {
        this.id = id;
    }
}
