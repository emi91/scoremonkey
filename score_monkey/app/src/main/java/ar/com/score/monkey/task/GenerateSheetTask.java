package ar.com.score.monkey.task;

import android.content.Context;

import com.google.inject.Inject;

import java.util.Date;

import ar.com.score.monkey.client.AndroidClient;
import ar.com.score.monkey.client.Bus;
import ar.com.score.monkey.dao.SheetDAO;
import ar.com.score.monkey.http.GenerateSheet;
import ar.com.score.monkey.http.GenerateSheetResponse;
import ar.com.score.monkey.model.RecordedSound;
import ar.com.score.monkey.model.Sheet;
import ar.com.score.monkey.task.event.GeneratedSheetEvent;
import ar.com.score.monkey.utils.DateUtils;
import ar.com.score.monkey.utils.FileManager;

/**
 * Created by Emiliano on 06/05/2015.
 */
public class GenerateSheetTask extends RestAsyncTask<Sheet> {
    @Inject
    private AndroidClient client;
    @Inject
    public Bus bus;
    private RecordedSound recordedSound;
    private String minor;
    private String major;
    private String clef;
    private String key;
    private String name;

    public GenerateSheetTask(Context context, RecordedSound recordedSound,String minor,String major,String clef,String key,String name) {
        super(context);
        this.recordedSound=recordedSound;
        this.key=key;
        this.minor=minor;
        this.major=major;
        this.clef=clef;
        this.name=name;

    }

    @Override
    public Sheet call() throws Exception {
        GenerateSheet generateSheet=new GenerateSheet(recordedSound.getTempo(),minor,major,key,clef,recordedSound.getPathToFile());
        String fileName=name+".mid";
        GenerateSheetResponse response=client.getGeneratedSheet(getContext(),generateSheet);
        Sheet sheet=new Sheet();
        sheet.setMusicxmlUrl(response.getMusicxml());
        sheet.setScoreUrl(response.getScore());

        sheet.setDate(DateUtils.format(new Date(), DateUtils.DDMMYYYYHHMM_DATE_FORMAT));
        sheet.setId(new Date().getTime());
        FileManager.downloadFileToInternalStorage(context, response.getMidi(), fileName);
        sheet.setMidiPath(fileName);
        sheet.setName(name);
        new SheetDAO().save(getContext(),sheet);
        return sheet;
    }


    @Override
    protected void onSuccess(Sheet sheet) throws Exception {
        super.onSuccess(sheet);
        bus.getBus().post(new GeneratedSheetEvent(sheet));
    }

    @Override
    protected void onException(Exception e) throws RuntimeException {

    }


}
