package ar.com.score.monkey.task.event;

/**
 * Created by Emiliano on 8/10/15.
 */
public class OnRequestError {
    public String error;

    public OnRequestError(String error) {
        this.error = error;
    }
}
