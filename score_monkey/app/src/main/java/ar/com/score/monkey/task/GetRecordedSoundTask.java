package ar.com.score.monkey.task;

import android.content.Context;

import com.google.inject.Inject;

import java.util.List;

import ar.com.score.monkey.client.AndroidClient;
import ar.com.score.monkey.client.Bus;
import ar.com.score.monkey.dao.RecordedSoundDAO;
import ar.com.score.monkey.model.RecordedSound;
import ar.com.score.monkey.module.OnGetRecordedSoundsEvent;
import roboguice.util.Strings;

/**
 * Created by Emiliano on 06/05/2015.
 */
public class GetRecordedSoundTask extends RestAsyncTask<List<RecordedSound>> {
    @Inject
    private AndroidClient client;
    @Inject
    public Bus bus;
    private String filterString;

    public GetRecordedSoundTask(Context context,String filterString) {
        super(context);
        this.filterString=filterString;
    }

    @Override
    public List<RecordedSound> call() throws Exception {
        List<RecordedSound> recordedSounds;
        if(!Strings.isEmpty(filterString))
            recordedSounds=new RecordedSoundDAO().getAllMatching(context,filterString,"name","date");
        else
            recordedSounds=new RecordedSoundDAO().getAll(context);
        return recordedSounds;
    }


    @Override
    protected void onSuccess(List<RecordedSound> recordedSounds) throws Exception {
        super.onSuccess(recordedSounds);
        bus.getBus().post(new OnGetRecordedSoundsEvent(recordedSounds));

    }

    @Override
    protected void onException(Exception e) throws RuntimeException {

    }


}
