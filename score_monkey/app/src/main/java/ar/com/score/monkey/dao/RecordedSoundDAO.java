package ar.com.score.monkey.dao;


import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.Date;
import java.util.List;

import ar.com.score.monkey.model.RecordedSound;
import ar.com.score.monkey.model.Song;
import ar.com.score.monkey.utils.Lists;

/**
 * Created by Emiliano on 3/8/15.
 */
public class RecordedSoundDAO extends BaseSharedPreferenceDAO<RecordedSound> {
    private final static String RECORDED_SOUND_KEY="recordedSoundKey";

    @Override
    public String getListName() {
        return RECORDED_SOUND_KEY;
    }

    @Override
    public Class getCollectionClazz() {
        return RecordedSound[].class;
    }
}
