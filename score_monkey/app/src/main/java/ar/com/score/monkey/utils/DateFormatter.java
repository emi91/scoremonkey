package ar.com.score.monkey.utils;

import android.content.Context;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Emiliano on 27/12/2014.
 */
public class DateFormatter {

    public static String getFormatterDate(String timeInSegs,Context context){
        Date date= new Date(Long.parseLong(timeInSegs)*1000);

        DateFormat dateFormat = android.text.format.DateFormat.getLongDateFormat(context);
        String s = dateFormat.format(date);
        return s.toUpperCase();
    }
    public static int getNowDayInInt(){
        Date date=new Date();
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_WEEK);
    }
}
