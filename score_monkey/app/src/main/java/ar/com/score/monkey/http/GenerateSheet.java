package ar.com.score.monkey.http;

/**
 * Created by Emiliano on 27/8/15.
 */
public class GenerateSheet {
    private String tempo;
    private String minor;
    private String major;
    private String key;
    private String clef;
    private String filePath;

    public GenerateSheet(String tempo, String minor, String major, String key, String clef, String filePath) {
        this.tempo = tempo;
        this.minor = minor;
        this.major = major;
        this.key = key;
        this.clef = clef;
        this.filePath = filePath;
    }

    public String getTempo() {
        return tempo;
    }

    public void setTempo(String tempo) {
        this.tempo = tempo;
    }

    public String getMinor() {
        return minor;
    }

    public void setMinor(String minor) {
        this.minor = minor;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getClef() {
        return clef;
    }

    public void setClef(String clef) {
        this.clef = clef;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}
