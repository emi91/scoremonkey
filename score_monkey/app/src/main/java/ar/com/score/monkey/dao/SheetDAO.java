package ar.com.score.monkey.dao;

import android.content.Context;

import java.util.Date;
import java.util.List;

import ar.com.score.monkey.model.RecordedSound;
import ar.com.score.monkey.model.Sheet;
import ar.com.score.monkey.model.Song;
import ar.com.score.monkey.utils.Lists;

/**
 * Created by Emiliano on 3/8/15.
 */
public class SheetDAO extends BaseSharedPreferenceDAO<Sheet> {
    private final static String SHEET_KEY="sheetKey";
    public List<Sheet> findByName(String name){
        //TODO implement
        return null;
    }

    @Override
    public String getListName() {
        return SHEET_KEY;
    }

    @Override
    public Class getCollectionClazz() {
        return Sheet[].class;
    }
}
