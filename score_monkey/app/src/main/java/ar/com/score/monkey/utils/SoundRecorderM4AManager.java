package ar.com.score.monkey.utils;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaRecorder;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import ar.com.score.monkey.model.Sheet;

/**
 * Created by Emiliano on 3/6/15.
 */
public class SoundRecorderM4AManager {
    private boolean isRecording;
    private FileOutputStream fos;
    private MediaRecorder recorder;
    private MediaPlayer mediaPlayer;
    private MediaPlayer.OnCompletionListener completeListener;

    public SoundRecorderM4AManager(){

    }

    public void playSound(Context context,String audioFilename){
        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnCompletionListener(completeListener);
            FileInputStream fd = context.openFileInput(audioFilename);
            if (fd != null) {
                mediaPlayer.setDataSource(fd.getFD());
                mediaPlayer.prepare();
                mediaPlayer.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startRecord(Context context,String audioFilename){
        recorder=new MediaRecorder();
        try {
            fos = context.openFileOutput(audioFilename, Context.MODE_PRIVATE);
        } catch (Exception e) {
            e.getMessage();
        }
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        try {
            recorder.setOutputFile(fos.getFD());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            recorder.prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        recorder.start();
        setIsRecording(!isRecording());
    }

    public void stopRecording(){
        if(recorder!=null){
            recorder.stop();
            recorder.reset();   // You can reuse the object by going back to setAudioSource() step
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        setIsRecording(!isRecording());
    }

    public void stopPlaying(){
        if(mediaPlayer!=null)
            mediaPlayer.stop();
    }

    public boolean isRecording() {
        return isRecording;
    }

    public void setIsRecording(boolean isRecording) {
        this.isRecording = isRecording;
    }

    public void setCompleteListener(MediaPlayer.OnCompletionListener completeListener) {
        this.completeListener = completeListener;
    }

    public boolean isPlaying() {
        return mediaPlayer.isPlaying();
    }
}
