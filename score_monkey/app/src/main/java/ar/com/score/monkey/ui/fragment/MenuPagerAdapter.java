package ar.com.score.monkey.ui.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class MenuPagerAdapter extends FragmentPagerAdapter {
    private List<BaseFragment> fragments;
	public MenuPagerAdapter(FragmentManager fm) {
		super(fm);
        fragments=new ArrayList<BaseFragment>();

        fragments.add(new RecordFragment());
        fragments.add(new RecordedSoundFragment());
        fragments.add(new SheetsFragment());
	}

    private String restaurantId;
	@Override
	public Fragment getItem(int position) {
		return fragments.get(position);
	}

	@Override
	public int getCount() {
		return fragments.size();
	}
}