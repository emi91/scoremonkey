package ar.com.score.monkey.task.event;

import ar.com.score.monkey.model.Sheet;

/**
 * Created by Emiliano on 4/8/15.
 */
public class SavedSheetEvent {
    public Sheet sheet;

    public SavedSheetEvent(Sheet sheet) {
        this.sheet = sheet;
    }
}
