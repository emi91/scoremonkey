package ar.com.score.monkey.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Emiliano on 27/8/15.
 */
public enum ArmorKey {
    ABM("Abm"),
    EBM("Ebm"),
    BBM("Bbm"),
    FM("Fm"),
    CM("Cm"),
    GM("Gm"),
    DM("Dm"),
    AM("Am"),
    EM("Em"),
    BM("Bm"),
    FMS("F#m"),
    CMS("C#m"),
    GMS("G#m"),
    DMS("D#m"),
    AMS("A#m"),
    CB("Cb"),
    GB("Gb"),
    DB("Db"),
    AB("Ab"),
    EB("Eb"),
    BB("Bb"),
    F("F"),
    C("C"),
    G("G"),
    D("D"),
    A("A"),
    E("E"),
    B("B"),
    FS("F#"),
    CS("C#");

    private String id;

    private ArmorKey(String id){
        this.id=id;
    }

    public String getId() {
        return id;
    }

    public static List<String> getArmorKeyNames(){
        List<String> names=new ArrayList<>();
        for (ArmorKey armorKey:ArmorKey.values()){
            names.add(armorKey.getId());
        }
        return names;
    }

}
