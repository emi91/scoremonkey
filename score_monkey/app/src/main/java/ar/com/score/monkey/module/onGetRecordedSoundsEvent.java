package ar.com.score.monkey.module;

import java.util.List;

import ar.com.score.monkey.model.RecordedSound;
import ar.com.score.monkey.model.Sheet;
import ar.com.score.monkey.model.Song;

/**
 * Created by Emiliano on 07/05/2015.
 */
public class OnGetRecordedSoundsEvent {
    public List<RecordedSound> items;

    public OnGetRecordedSoundsEvent(List<RecordedSound> items) {
        this.items = items;
    }
}
