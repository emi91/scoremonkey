package ar.com.score.monkey.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.inject.Inject;

import ar.com.score.monkey.R;
import ar.com.score.monkey.client.Bus;
import ar.com.score.monkey.model.Sheet;
import ar.com.score.monkey.task.event.ActivityResultEvent;
import ar.com.score.monkey.task.event.SavedSheetEvent;
import ar.com.score.monkey.task.event.SoundRecordedEvent;
import ar.com.score.monkey.ui.activity.HomeActivity;
import ar.com.score.monkey.ui.view.CheckableGroupController;
import ar.com.score.monkey.utils.ExtraConstants;
import ar.com.score.monkey.utils.Lists;
import roboguice.inject.InjectView;

/**
 * Created by emi91_000 on 23/12/2014.
 */
public class HomeFragment extends BaseFragment {
    @Inject
    private Bus bus;
    private CustomViewPager viewPager;
    private PagerAdapter productDetailPagerAdapter;
    @InjectView(R.id.radio)
    private View radio;
    @InjectView(R.id.programation)
    private View programation;
    @InjectView(R.id.sheets_list)
    private View sheetsList;
    private CheckableGroupController checkableGroupController;
    private static final String SELECTED="selected";
    private static int selected=-1;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_fragment,container,false);
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.getBus().registerSticky(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        bus.getBus().unregister(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        viewPager = (CustomViewPager) getView().findViewById(R.id.view_pager);
        productDetailPagerAdapter = new MenuPagerAdapter(
                HomeFragment.this.getChildFragmentManager());
        viewPager.setAdapter(productDetailPagerAdapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                checkableGroupController.setSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        initMenu();
        if(savedInstanceState!=null){
            if(selected==-1)
                checkableGroupController.setSelected(savedInstanceState.getInt(SELECTED));
            else
                checkableGroupController.setSelected(selected);
        }
    }

    private void initMenu() {
        checkableGroupController=new CheckableGroupController(Lists.newArrayList(radio,programation, sheetsList));
        ((HomeActivity)getActivity()).hideSearch();
        checkableGroupController.setOnSelectListener(new CheckableGroupController.OnSelectedListener() {
            @Override
            public void onItemSelected(View view, int position) {
                viewPager.setCurrentItem(position);
                if(position==0)
                    ((HomeActivity)getActivity()).hideSearch();
                else
                    ((HomeActivity)getActivity()).showSearch();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SELECTED, checkableGroupController.getSelected());
    }

    public boolean isRecordFragmentVisible(){
        return viewPager.getCurrentItem()==0;
    }

    public void onEvent(SoundRecordedEvent event){
        selected=1;
        checkableGroupController.setSelected(1);
    }

    public void onEvent(ActivityResultEvent event) {
        if (event.requestCode == RecordedSoundFragment.GENERATE_SHEET_REQ && event.resultCode == Activity.RESULT_OK){
            Sheet sheet= (Sheet) event.data.getSerializableExtra(ExtraConstants.SHEET);
            bus.getBus().postSticky(new SavedSheetEvent(sheet));
            checkableGroupController.setSelected(2);
            selected=2;
        }
    }
}
