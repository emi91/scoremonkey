package ar.com.score.monkey.ui.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;


import java.io.File;

import ar.com.score.monkey.R;
import ar.com.score.monkey.utils.ExtraConstants;

/**
 * Created by emi91_000 on 29/05/2015.
 */
public class CustomInputDialogFragment extends DialogFragment {

    private OnTextInputSubmitListener callback;

    public interface OnTextInputSubmitListener{
        public void onTextSubmit(String text);
    }

    public static CustomInputDialogFragment newInstance(String title, String hint){
        return newInstance(title,hint, false);
    }

    public static CustomInputDialogFragment newInstance(String title, String hint,boolean number) {
        CustomInputDialogFragment frag = new CustomInputDialogFragment();
        Bundle args = new Bundle();
        args.putString(ExtraConstants.DIALOG_TITLE, title);
        args.putString("hint", hint);
        args.putBoolean("numeric",number);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        SpannableString title = new SpannableString(getArguments().getString(ExtraConstants.DIALOG_TITLE));
        builder.setTitle(title);

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.custom_input_dialog, null);

        builder.setView(view);

        builder.setPositiveButton("Aceptar", null);

        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog d = builder.create();

        d.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {

                final EditText input = (EditText) getDialog().findViewById(R.id.input);

                input.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                    }
                });
                input.requestFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                Button b = d.getButton(AlertDialog.BUTTON_POSITIVE);
                Button c = d.getButton(AlertDialog.BUTTON_NEGATIVE);
                c.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                        dialog.dismiss();
                    }
                });
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something

                        final EditText input = (EditText) getDialog().findViewById(R.id.input);

                        String inputText = input.getText().toString();

                        if (inputText.trim().length() == 0) {
                            input.setError(getString(R.string.empty_field_error));
                            return;
                        }

                        callback.onTextSubmit(inputText);
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
                        dialog.dismiss();
                    }
                });
            }
        });

        return d;
    }

    @Override
    public void setTargetFragment(Fragment fragment, int requestCode) {
        super.setTargetFragment(fragment, requestCode);
        callback = (OnTextInputSubmitListener) fragment;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);

    }
}
