package ar.com.score.monkey.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import ar.com.score.monkey.R;
import ar.com.score.monkey.dao.RecordedSoundDAO;
import ar.com.score.monkey.dao.SheetDAO;
import ar.com.score.monkey.model.RecordedSound;
import ar.com.score.monkey.model.Sheet;
import ar.com.score.monkey.utils.DateUtils;

/**
 * Created by emi91_000 on 17/11/2014.
 */
public class SheetAdapter extends BaseRecyclerViewAdapter<SheetHolder, Sheet> implements ItemTouchHelperAdapter {

    private Context context;

    public SheetAdapter(Context context, List<Sheet> items){
        this.context = context;
        this.itemList = items;
    }

    @Override
    public SheetHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new SheetHolder(LayoutInflater.from(context).inflate(R.layout.sheet_item, viewGroup, false));
    }

    @Override
    public void onBindBaseViewHolder(SheetHolder holder, int position, Sheet item) {
        if(item!=null){
            holder.title.setText(item.getName());
            holder.description.setText(item.getDate());

        }
    }
    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        Collections.swap(itemList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int position) {
        new SheetDAO().removeObject(context, itemList.get(position));
        itemList.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
        if (itemList.size()==0 && onEmptyList!=null)
            this.onEmptyList.onEmptyList();
        Toast.makeText(context, "Partitura eliminada.", Toast.LENGTH_SHORT).show();
    }


    public void setNewItemList(List<Sheet> items){
        this.itemList = items;
        notifyDataSetChanged();
    }
}
