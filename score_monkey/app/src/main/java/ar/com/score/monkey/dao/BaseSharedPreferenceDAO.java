package ar.com.score.monkey.dao;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import ar.com.score.monkey.model.RecordedSound;
import ar.com.score.monkey.utils.ExtraConstants;
import ar.com.score.monkey.utils.Lists;
import ar.com.score.monkey.utils.StringUtils;

/**
 * Created by Emiliano on 3/8/15.
 */
public abstract class BaseSharedPreferenceDAO<T> {
    private Gson gson;

    public BaseSharedPreferenceDAO() {
        this.gson = new Gson();
    }

    public String serialize(Object object){
        return gson.toJson(object);
    }
    public <E> E deserialize(String string,Class<E> clazz){
        return gson.fromJson(string, clazz);
    }


    public void save(Context context,T object){
        List<T> allObjects=getAll(context);
        if(Lists.isNullOrEmpty(allObjects))
            allObjects=Lists.newArrayList();
        allObjects.add(object);

        saveobjectsList(context, allObjects);
    }

    public List<T> getAll(Context context){
        SharedPreferences settings = context.getSharedPreferences(ExtraConstants.SHARED_PREFERENCES, 0);
        String recordedSoundsString=settings.getString(getListName(),"");
        if(StringUtils.isEmpty(recordedSoundsString)){
            return Lists.newArrayList();
        }
        return Lists.convertArrayToList(deserialize(recordedSoundsString, getCollectionClazz()));
    }

    public List<T> getAllMatching(Context context,String stringFilter,String...fieldNames){
        List<T> list=getAll(context);
        List<T> listToReturn=new ArrayList<T>();
        for(T each:list){
            try {
                for(String fieldName:fieldNames){
                    if(containsValueInField(each,fieldName,stringFilter)){
                        listToReturn.add(each);
                        break;
                    }
                }

            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
        }

        return listToReturn;
    }

    public boolean containsValueInField(Object each,String fieldName,String stringFilter) throws NoSuchFieldException {
        String field= (String) runGetter(each.getClass().getDeclaredField(fieldName),each);
        return field.toLowerCase().contains(stringFilter.toLowerCase());
    }

    public static Object runGetter(Field field, Object o)
    {
        // MZ: Find the correct method
        for (Method method : o.getClass().getMethods())
        {
            if ((method.getName().startsWith("get")) && (method.getName().length() == (field.getName().length() + 3)))
            {
                if (method.getName().toLowerCase().endsWith(field.getName().toLowerCase()))
                {
                    // MZ: Method found, run it
                    try
                    {
                        return method.invoke(o);
                    }
                    catch (IllegalAccessException e)
                    {
                    }
                    catch (InvocationTargetException e)
                    {
                    }

                }
            }
        }


        return null;
    }


    public void removeObject(Context context, T object){
        List<T> allObjects=getAll(context);
        allObjects.remove(object);
        saveobjectsList(context,allObjects);
    }

    public abstract String getListName();


    public abstract Class<T[]> getCollectionClazz();

    private void saveobjectsList(Context context, List<T> objectsList) {
        SharedPreferences.Editor editor = context.getSharedPreferences(ExtraConstants.SHARED_PREFERENCES, 0).edit();
        editor.putString(getListName(), serialize(objectsList));
        editor.commit();
    }
}
