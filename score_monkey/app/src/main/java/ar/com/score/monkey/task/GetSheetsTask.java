package ar.com.score.monkey.task;

import android.content.Context;

import com.google.inject.Inject;

import java.util.List;

import ar.com.score.monkey.client.AndroidClient;
import ar.com.score.monkey.client.Bus;
import ar.com.score.monkey.dao.SheetDAO;
import ar.com.score.monkey.model.Sheet;
import ar.com.score.monkey.module.OnGetSheetsEvent;
import roboguice.util.Strings;

/**
 * Created by Emiliano on 06/05/2015.
 */
public class GetSheetsTask extends RestAsyncTask<List<Sheet>> {
    @Inject
    private AndroidClient client;
    @Inject
    public Bus bus;
    private static final String SHARED_PREF_NAME="arComRadioConcepto";
    public static final String ITEMS_CACHED="itemsCached";
    private String filterString;

    public GetSheetsTask(Context context) {
        super(context);
    }

    public GetSheetsTask(Context context,String filterString) {
        super(context);
        this.filterString=filterString;
    }

    @Override
    public List<Sheet> call() throws Exception {
        List<Sheet> sheets;
        if(!Strings.isEmpty(filterString))
             sheets=new SheetDAO().getAllMatching(context, filterString,"name","date");
        else
            sheets=new SheetDAO().getAll(context);
        return sheets;
    }


    @Override
    protected void onSuccess(List<Sheet> sheets) throws Exception {
        super.onSuccess(sheets);
        bus.getBus().post(new OnGetSheetsEvent(sheets));

    }

    @Override
    protected void onException(Exception e) throws RuntimeException {

    }

    public void setFilterString(String filterString) {
        this.filterString = filterString;
    }
}
