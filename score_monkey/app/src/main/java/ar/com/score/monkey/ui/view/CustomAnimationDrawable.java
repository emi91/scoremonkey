package ar.com.score.monkey.ui.view;

import android.graphics.drawable.AnimationDrawable;

/**
 * Created by Emiliano on 11/05/2015.
 */
public  class CustomAnimationDrawable extends AnimationDrawable {
    public OnSecondFrame onSecondFrame;

    public void setOnSecondFrame(OnSecondFrame onSecondFrame) {
        this.onSecondFrame = onSecondFrame;
    }

    @Override
    public boolean selectDrawable(int idx) {
        boolean value= super.selectDrawable(idx);
        if(onSecondFrame!=null&& idx==1){
            onSecondFrame.onSecondFrame();
        }
        return value;
    }

    public interface OnSecondFrame{
        public void onSecondFrame();
    }
}