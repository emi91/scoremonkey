package ar.com.score.monkey.ui.adapter;

import android.support.v7.widget.RecyclerView;

import java.util.List;

/**
 * Created by ignacio on 16/04/2015.
 */
public abstract class BaseRecyclerViewAdapter<T extends BaseViewHolder,E> extends RecyclerView.Adapter {
    protected OnItemClickListener<E> onItemClickListener;
    protected List<E> itemList;
    protected OnEmptyList onEmptyList;
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        onBindBaseViewHolder((T) holder, position, itemList.get(position));
        ((T) holder).setOnClickListener(new BaseViewHolder.OnViewHolderClickListener() {
            @Override
            public void onViewHolderClickListener(BaseViewHolder holder) {
                if(onItemClickListener!=null)
                onItemClickListener.onItemClickListener(itemList.get(position),position);
            }
        });
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener<E> {
        public void onItemClickListener(E object, int position);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public abstract void onBindBaseViewHolder(T holder, int position,E item);

    public static interface OnEmptyList{
        public void onEmptyList();
    }

    public void setOnEmptyList(OnEmptyList onEmptyList) {
        this.onEmptyList = onEmptyList;
    }
}
