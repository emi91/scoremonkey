package ar.com.score.monkey.client;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import de.greenrobot.event.EventBus;

/**
 * Created by Emiliano on 07/05/2015.
 */
@Singleton
public class Bus {
    @Inject
    public EventBus bus;

    public EventBus getBus() {
        return bus;
    }

    public void setBus(EventBus bus) {
        this.bus = bus;
    }
}
