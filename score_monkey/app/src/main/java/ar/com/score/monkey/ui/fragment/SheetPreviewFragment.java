package ar.com.score.monkey.ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.Toast;

import ar.com.score.monkey.R;
import ar.com.score.monkey.model.Sheet;
import ar.com.score.monkey.ui.activity.SheetPreviewActivity;
import ar.com.score.monkey.ui.view.LoadingLayout;
import ar.com.score.monkey.utils.ExtraConstants;
import ar.com.score.monkey.utils.SoundRecorderM4AManager;
import roboguice.inject.InjectView;

/**
 * Created by Emiliano on 27/8/15.
 */
public class SheetPreviewFragment extends BaseFragment {
    @InjectView(R.id.sheet_preview)
    private WebView webView;
    @InjectView(R.id.loading_layout)
    private LoadingLayout loadingLayout;
    private SoundRecorderM4AManager soundRecorderM4AManager;

    private Sheet sheet;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sheet_preview_fragment,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sheet= (Sheet) getArguments().getSerializable(ExtraConstants.RECORDED_SOUND);

        soundRecorderM4AManager = new SoundRecorderM4AManager();
        webView.getSettings().setJavaScriptEnabled(true); // enable javascript
        webView.setWebChromeClient(new WebChromeClient());

        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);

        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setAppCacheEnabled(true);

        webView.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(getActivity(), description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                loadingLayout.stopLoading();

                webView.loadUrl("javascript:(function() { " +
                        "\n" +
                        "$('#open_file_btn').remove()\n" +
                        "$('#go_url_input').remove()\n" +
                        "$('#go_url_btn').remove()\n" +
                        "$('#standalone_btn').remove()" +
                        "})()");
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if(Uri.parse(url).getHost().length() == 0) {
                    return false;
                }

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                view.getContext().startActivity(intent);
                return false;
            }


        });
        webView.loadUrl(sheet.getScoreUrl());
    }

    public void playMidi(){
        soundRecorderM4AManager.setCompleteListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                ((SheetPreviewActivity) getActivity()).stopMidiInFragment();
            }
        });
        soundRecorderM4AManager.playSound(getActivity(), sheet.getMidiPath());
    }

    public void stopMidi() {
        soundRecorderM4AManager.stopPlaying();
    }

    public void shareSheet() {
        invokeShare(getActivity(),"Mira lo que compuse: " , "Cómo deseas compartir la partitura?");
    }
    public void invokeShare(Activity activity, String quote, String credit) {
        Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, quote);
        shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, sheet.getScoreUrl());

                activity.startActivity(Intent.createChooser(shareIntent, credit));
    }

    @Override
    public void onStop() {
        super.onStop();
        if(soundRecorderM4AManager!=null)
            soundRecorderM4AManager.stopPlaying();
    }
}
