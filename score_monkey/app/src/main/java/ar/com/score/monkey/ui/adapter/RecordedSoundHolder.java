package ar.com.score.monkey.ui.adapter;

/**
 * Created by Emiliano on 28/04/2015.
 */

import android.view.View;
import android.widget.TextView;

import ar.com.score.monkey.R;

public class RecordedSoundHolder extends BaseViewHolder {
    TextView title;
    TextView description;

    public RecordedSoundHolder(View itemView) {
        super(itemView);
        title= (TextView) itemView.findViewById(R.id.title);
        description= (TextView) itemView.findViewById(R.id.description);
    }
}
