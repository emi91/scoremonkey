package ar.com.score.monkey.client;


import ar.com.score.monkey.http.GenerateSheetResponse;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedString;

public interface IAndroidAuthorityClient {
    @Multipart
    @POST("/melodies")
    public GenerateSheetResponse getGeneratedSheet(@Part("audio")TypedFile audio,@Part("minor")TypedString minor,@Part("major")TypedString major,@Part("tempo")TypedString tempo,@Part("key")TypedString key,@Part("clef")TypedString clef);

}
