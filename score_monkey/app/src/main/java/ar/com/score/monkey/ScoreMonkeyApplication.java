package ar.com.score.monkey;

import android.app.Application;

import ar.com.score.monkey.module.ApplicationModuleProvider;
import com.google.inject.util.Modules;

import roboguice.RoboGuice;


public class ScoreMonkeyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        RoboGuice.setUseAnnotationDatabases(true);
        RoboGuice.getOrCreateBaseApplicationInjector(this, RoboGuice.DEFAULT_STAGE,
                                                     Modules.override(RoboGuice.newDefaultRoboModule(this)).with(new ApplicationModuleProvider(this)));

    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }

    private static boolean activityVisible;


}
