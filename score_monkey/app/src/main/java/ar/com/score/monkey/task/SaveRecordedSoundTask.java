package ar.com.score.monkey.task;

import android.content.Context;

import com.google.inject.Inject;

import java.util.List;

import ar.com.score.monkey.client.AndroidClient;
import ar.com.score.monkey.client.Bus;
import ar.com.score.monkey.dao.RecordedSoundDAO;
import ar.com.score.monkey.model.RecordedSound;
import ar.com.score.monkey.module.OnGetRecordedSoundsEvent;
import ar.com.score.monkey.task.event.SoundRecordedEvent;

/**
 * Created by Emiliano on 06/05/2015.
 */
public class SaveRecordedSoundTask extends RestAsyncTask<RecordedSound> {
    @Inject
    private AndroidClient client;
    @Inject
    public Bus bus;
    private RecordedSound recordedSound;

    public SaveRecordedSoundTask(Context context,RecordedSound recordedSound) {
        super(context);
        this.recordedSound=recordedSound;
    }

    @Override
    public RecordedSound call() throws Exception {
        new RecordedSoundDAO().save(getContext(), recordedSound);
        return recordedSound;
    }


    @Override
    protected void onSuccess(RecordedSound recordedSound) throws Exception {
        super.onSuccess(recordedSound);
        bus.getBus().post(new SoundRecordedEvent(recordedSound.getId()));
    }

    @Override
    protected void onException(Exception e) throws RuntimeException {

    }


}
