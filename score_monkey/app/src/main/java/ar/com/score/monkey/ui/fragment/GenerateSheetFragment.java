package ar.com.score.monkey.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.inject.Inject;

import java.util.List;

import ar.com.score.monkey.R;
import ar.com.score.monkey.client.Bus;
import ar.com.score.monkey.model.ArmorKey;
import ar.com.score.monkey.model.BarGenerator;
import ar.com.score.monkey.model.RecordedSound;
import ar.com.score.monkey.model.SongClef;
import ar.com.score.monkey.task.GenerateSheetTask;
import ar.com.score.monkey.task.event.GeneratedSheetEvent;
import ar.com.score.monkey.task.event.OnRequestError;
import ar.com.score.monkey.ui.activity.commons.BaseActivity;
import ar.com.score.monkey.ui.view.CheckableGroupController;
import ar.com.score.monkey.ui.view.LoadingLayout;
import ar.com.score.monkey.utils.ExtraConstants;
import ar.com.score.monkey.utils.Lists;
import roboguice.inject.InjectView;

/**
 * Created by Emiliano on 28/04/2015.
 */
public class GenerateSheetFragment extends BaseFragment {
    @Inject
    private Bus bus;
    @InjectView(R.id.major)
    private Spinner majorSpinner;
    @InjectView(R.id.minor)
    private Spinner minorSpinner;
    @InjectView(R.id.armor)
    private Spinner armorSpinner;


    @InjectView(R.id.sol_clef)
    private LinearLayout solClefView;
    @InjectView(R.id.fa_clef)
    private LinearLayout faClefView;
    @InjectView(R.id.do_clef)
    private LinearLayout doClefView;
    @InjectView(R.id.generateSheet)
    private Button generateSheetButton;
    @InjectView(R.id.name)
    private EditText name;

    @InjectView(R.id.loading_layout)
    private LoadingLayout loadingLayout;

    private CheckableGroupController checkableGroupController;
    private SongClef currentSongClef;
    private static final String SELECTED="selected";
    private RecordedSound recordedSound;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.generate_sheet_fragment, container, false);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        recordedSound= (RecordedSound) getArguments().getSerializable(ExtraConstants.RECORDED_SOUND);
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.getBus().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        bus.getBus().unregister(this);
    }

    // add items into spinner dynamically

    private void initializeAdapter(List<String> list,Spinner spinner) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }


    public void initializeSpinners() {
        initializeAdapter(BarGenerator.getMajorValue(), majorSpinner);
        majorSpinner.setSelection(3);
        initializeAdapter(BarGenerator.getMinorValues(), minorSpinner);
        minorSpinner.setSelection(2);
        initializeAdapter(ArmorKey.getArmorKeyNames(), armorSpinner);
        armorSpinner.setSelection(22);

    }
    private void initMenu() {
        currentSongClef=SongClef.G;
        checkableGroupController=new CheckableGroupController(Lists.<View>newArrayList(solClefView, faClefView, doClefView));
        checkableGroupController.setOnSelectListener(new CheckableGroupController.OnSelectedListener() {
            @Override
            public void onItemSelected(View view, int position) {
                currentSongClef = SongClef.getSongClefByPosition(position);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadingLayout.stopLoading();
        initializeSpinners();
        initMenu();
        generateSheetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (name.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Debe ingresar un nombre para la partitura", Toast.LENGTH_LONG).show();
                    return;
                }
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                new GenerateSheetTask(getActivity(), recordedSound, getSpinnerValue(minorSpinner), getSpinnerValue(majorSpinner), currentSongClef.name(), getSpinnerValue(armorSpinner), name.getText().toString()).execute();
                loadingLayout.startLoading();
            }
        });
        if(savedInstanceState!=null){
            checkableGroupController.setSelected(savedInstanceState.getInt(SELECTED));
        }

    }

    public String getSpinnerValue(Spinner spinner){
        return spinner.getSelectedItem().toString();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(SELECTED, checkableGroupController.getSelected());
    }

    public void onEvent(GeneratedSheetEvent event){
        Intent intent=new Intent();
        intent.putExtra(ExtraConstants.SHEET, event.sheet);
        getActivity().setResult(Activity.RESULT_OK, intent);
        getActivity().finish();
    }

    public void onEvent(final OnRequestError event){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadingLayout.stopLoadingOnUIThread((BaseActivity) getActivity());
                Toast.makeText(getActivity(), event.error, Toast.LENGTH_LONG).show();
            }
        });

    }

}
