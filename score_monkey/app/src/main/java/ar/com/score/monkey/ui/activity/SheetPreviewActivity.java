package ar.com.score.monkey.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import ar.com.score.monkey.R;
import ar.com.score.monkey.ui.activity.commons.BaseActivity;
import ar.com.score.monkey.ui.activity.commons.BaseActivityWithToolBar;
import ar.com.score.monkey.ui.activity.commons.BaseActivityWithoutToolbar;
import ar.com.score.monkey.ui.fragment.BaseFragment;
import ar.com.score.monkey.ui.fragment.SheetPreviewFragment;
import roboguice.inject.InjectView;

public class SheetPreviewActivity extends BaseActivityWithToolBar {

    private boolean playing;
    private ImageView playButton;
    @Override
    public Class<? extends BaseFragment> getFragmentClass() {
        return SheetPreviewFragment.class;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playButton= (ImageView) getToolbar().findViewById(R.id.button);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playing = !playing;
                if (playing)
                    playMidiInFragment();
                else
                    stopMidiInFragment();
            }
        });
        (getToolbar().findViewById(R.id.search)).setVisibility(View.GONE);
        (getToolbar().findViewById(R.id.share)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shareSheet();
            }
        });



    }

    private void playMidiInFragment() {
        SheetPreviewFragment sheetPreviewFragment = (SheetPreviewFragment) getSupportFragmentManager().findFragmentByTag(SheetPreviewFragment.class.getName());
        sheetPreviewFragment.playMidi();
        playButton.setImageResource(R.drawable.ic_ic_stop_big);
    }

    private void shareSheet() {
        SheetPreviewFragment sheetPreviewFragment = (SheetPreviewFragment) getSupportFragmentManager().findFragmentByTag(SheetPreviewFragment.class.getName());
        sheetPreviewFragment.shareSheet();
    }
    public void stopMidiInFragment(){
        SheetPreviewFragment sheetPreviewFragment= (SheetPreviewFragment) getSupportFragmentManager().findFragmentByTag(SheetPreviewFragment.class.getName());
        sheetPreviewFragment.stopMidi();
        playButton.setImageResource(R.drawable.ic_ic_play_arrow_big);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.home_activity;
    }
}
