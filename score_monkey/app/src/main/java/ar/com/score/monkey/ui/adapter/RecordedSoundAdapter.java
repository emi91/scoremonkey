package ar.com.score.monkey.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.inject.Inject;

import java.util.Collections;
import java.util.List;

import ar.com.score.monkey.R;
import ar.com.score.monkey.dao.RecordedSoundDAO;
import ar.com.score.monkey.model.RecordedSound;
import ar.com.score.monkey.utils.DateUtils;

/**
 * Created by emi91_000 on 17/11/2014.
 */
public class RecordedSoundAdapter extends BaseRecyclerViewAdapter<RecordedSoundHolder, RecordedSound> implements ItemTouchHelperAdapter {
    private Context context;

    public RecordedSoundAdapter(Context context, List<RecordedSound> items){
        this.context = context;
        this.itemList = items;
    }

    @Override
    public RecordedSoundHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new RecordedSoundHolder(LayoutInflater.from(context).inflate(R.layout.recorded_sound_item, viewGroup, false));
    }

    @Override
    public void onBindBaseViewHolder(RecordedSoundHolder holder, int position, RecordedSound item) {
        holder.title.setText(item.getName());
        holder.description.setText(item.getDate());
    }


    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        Collections.swap(itemList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int position) {
        new RecordedSoundDAO().removeObject(context, itemList.get(position));
        itemList.remove(position);
        notifyItemRemoved(position);
        notifyDataSetChanged();
        if (itemList.size()==0 && onEmptyList!=null)
            this.onEmptyList.onEmptyList();
        Toast.makeText(context,"Grabación eliminada.",Toast.LENGTH_SHORT).show();
    }

    public void setNewItemList(List<RecordedSound> items){
        this.itemList = items;
        notifyDataSetChanged();
    }

}
