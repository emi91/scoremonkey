package ar.com.score.monkey.ui.activity;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import ar.com.score.monkey.R;
import ar.com.score.monkey.ui.activity.commons.BaseActivityWithoutToolbar;
import ar.com.score.monkey.ui.fragment.BaseFragment;
import ar.com.score.monkey.ui.fragment.GenerateSheetFragment;

public class GenerateSheetActivity extends BaseActivityWithoutToolbar {

    @Override
    public Class<? extends BaseFragment> getFragmentClass() {
        return GenerateSheetFragment.class;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getToolbar().findViewById(R.id.button).setVisibility(View.GONE);
        getToolbar().findViewById(R.id.share).setVisibility(View.GONE);
        getToolbar().findViewById(R.id.search).setVisibility(View.GONE);
    }
    @Override
    protected int getLayoutResId() {
        return R.layout.home_activity;
    }
}
