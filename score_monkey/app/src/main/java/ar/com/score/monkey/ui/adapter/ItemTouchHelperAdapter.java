package ar.com.score.monkey.ui.adapter;

/**
 * Created by Emiliano on 5/8/15.
 */
public interface ItemTouchHelperAdapter {

    void onItemMove(int fromPosition, int toPosition);

    void onItemDismiss(int position);
}
