package ar.com.score.monkey.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.inject.Inject;

import java.util.List;

import ar.com.score.monkey.R;
import ar.com.score.monkey.client.Bus;
import ar.com.score.monkey.model.Sheet;
import ar.com.score.monkey.module.OnGetSheetsEvent;
import ar.com.score.monkey.task.GetRecordedSoundTask;
import ar.com.score.monkey.task.GetSheetsTask;
import ar.com.score.monkey.task.event.OnFilterChange;
import ar.com.score.monkey.task.event.SavedSheetEvent;
import ar.com.score.monkey.ui.activity.HomeActivity;
import ar.com.score.monkey.ui.activity.SheetPreviewActivity;
import ar.com.score.monkey.ui.adapter.BaseRecyclerViewAdapter;
import ar.com.score.monkey.ui.adapter.SheetAdapter;
import ar.com.score.monkey.ui.adapter.SimpleItemTouchHelperCallback;
import ar.com.score.monkey.ui.view.LoadingLayout;
import ar.com.score.monkey.utils.ExtraConstants;
import ar.com.score.monkey.utils.Lists;
import roboguice.inject.InjectView;

/**
 * Created by Emiliano on 28/04/2015.
 */
public class SheetsFragment extends BaseFragment implements BaseRecyclerViewAdapter.OnEmptyList {
    @Inject
    private Bus bus;
    @InjectView(R.id.list)
    private RecyclerView programationView;
    private Sheet newSheet;
    private List<Sheet> sheetList;
    @InjectView(R.id.emptyPlaceHolderList)
    private TextView emptyPlaceHolderList;
    @InjectView(R.id.loading_layout)
    private LoadingLayout loadingLayout;
    private GetSheetsTask getSheetsTask;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sheets_fragment,container,false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        programationView.setHasFixedSize(true);
        programationView.setLayoutManager(new LinearLayoutManager(getActivity(), GridLayoutManager.VERTICAL, false));
        if(getSheetsTask==null)
            getSheetsTask=new GetSheetsTask(getActivity());
        getSheetsTask.setFilterString(((HomeActivity) getActivity()).getFilterString());
        getSheetsTask.execute();
    }

    @Override
    public void onStart() {
        super.onStart();
        bus.getBus().registerSticky(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        bus.getBus().unregister(this);
    }

    public void onEvent(final OnGetSheetsEvent event) {
//        getActivity().runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
                sheetList = newSheet != null && !Lists.isNullOrEmpty(event.items) ? reorderList(event.items) : event.items;
                if (programationView.getAdapter() == null) {
                    SheetAdapter adapter = new SheetAdapter(getActivity(), sheetList);
                    adapter.setOnItemClickListener(new BaseRecyclerViewAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClickListener(Object object, int position) {
                            Intent intent = new Intent(getActivity(), SheetPreviewActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(ExtraConstants.RECORDED_SOUND, (Sheet) object);
                            intent.putExtras(bundle);
                            getActivity().startActivity(intent);
                        }
                    });
                    ItemTouchHelper.Callback callback =
                            new SimpleItemTouchHelperCallback(adapter);
                    ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
                    touchHelper.attachToRecyclerView(programationView);
                    adapter.setOnEmptyList(this);
                    programationView.setAdapter(adapter);
                } else {
                    ((SheetAdapter)programationView.getAdapter()).setNewItemList(sheetList);
                }
                if (Lists.isNullOrEmpty(sheetList)) {
                    emptyPlaceHolderList.setVisibility(View.VISIBLE);
                    programationView.setVisibility(View.GONE);
                } else {
                    emptyPlaceHolderList.setVisibility(View.GONE);
                    programationView.setVisibility(View.VISIBLE);
                }
                loadingLayout.stopLoading();
//            }
//        });
    }


    public void onEvent(final SavedSheetEvent event){
        if(newSheet==null || !newSheet.equals(event.sheet)){
            newSheet=event.sheet;
            if(getSheetsTask==null)
                getSheetsTask=new GetSheetsTask(getActivity(),null);
            getSheetsTask.execute();
        }
    }

    public List<Sheet> reorderList(List<Sheet> sheets){
        Sheet sheetToMove=null;

        for (Sheet sheet:sheets){
            if(sheet.getId().equals(newSheet.getId())){
                sheetToMove=sheet;
                break;
            }
        }
        if(sheetToMove!=null){
            sheets.remove(sheetToMove);

            List<Sheet> sheetsToReturn= Lists.newArrayList(sheetToMove);
            sheetsToReturn.addAll(sheets);
            return sheetsToReturn;
        }
        return sheets;
    }
    @Override
    public void onEmptyList() {
        emptyPlaceHolderList.setVisibility(View.VISIBLE);
        programationView.setVisibility(View.GONE);
    }
    public void onEvent(OnFilterChange event){
        new GetSheetsTask(getActivity(),event.string).execute();
    }
}
