package ar.com.score.monkey.task;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;


import java.net.ConnectException;
import java.net.SocketTimeoutException;

import javax.inject.Inject;

import ar.com.score.monkey.R;
import retrofit.RetrofitError;
import roboguice.util.RoboAsyncTask;

/**
 * AsyncTask to use with REST API Calls, it should handle HTTP Errors auto-magically.
 */
public abstract class RestAsyncTask<T> extends RoboAsyncTask<T> {


    @Inject
    protected RestAsyncTask(Context context) {
        super(context);
    }

    @Override
    protected void onPreExecute() throws Exception {
        super.onPreExecute();
    }

    @Override
    protected void onException(Exception e) throws RuntimeException {
        try {
            if(e instanceof RetrofitError) {
                RetrofitError retrofitError = (RetrofitError) e;
                if(retrofitError.getResponse() != null) {
                    if (retrofitError.getResponse().getStatus() > 500 ){
                        String msg = "Network error HTTP ("+retrofitError.getResponse().getStatus()+")";
                        if (retrofitError.getMessage()!=null && !retrofitError.getMessage().isEmpty()){
                            msg += ": "+retrofitError.getMessage();
                        }
                        super.onException(e);
                    }else if (retrofitError.getBody()==null){
                        Toast.makeText(this.context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }else if (retrofitError.getCause() instanceof ConnectException){
                        Toast.makeText(this.context, R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }else if (retrofitError.getCause() instanceof SocketTimeoutException){
                        Toast.makeText(this.context, R.string.connection_error, Toast.LENGTH_SHORT).show();
                    }else{
//						BufferedReader reader = new BufferedReader(new InputStreamReader(((RetrofitError) e).getResponse().getBody().in()));
//						SamiErrorDto errorDto = new Gson().fromJson(reader, ErrorDto.class);
//						onApiError(errorDto);
                    }
                }else if(retrofitError.getKind() == RetrofitError.Kind.NETWORK && !isCancelled()){
                    Toast.makeText(this.context, R.string.connection_error, Toast.LENGTH_SHORT).show();
                    super.onException(e);
                }
            }else {
                super.onException(e);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    @Override
    protected void onInterrupted(Exception e) {
        Log.d("BACKGROUND_TASK", "Interrupting background task " + this);
    }

    @Override
    protected void onFinally() throws RuntimeException {
        super.onFinally();
    }

}
