package ar.com.score.monkey.ui.fragment;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;

import java.io.File;
import java.io.IOException;
import java.util.List;

import ar.com.score.monkey.R;
import ar.com.score.monkey.client.Bus;
import ar.com.score.monkey.model.RecordedSound;
import ar.com.score.monkey.model.Sheet;
import ar.com.score.monkey.module.OnGetRecordedSoundsEvent;
import ar.com.score.monkey.task.GetRecordedSoundTask;
import ar.com.score.monkey.task.event.ActivityResultEvent;
import ar.com.score.monkey.task.event.OnFilterChange;
import ar.com.score.monkey.task.event.SavedSheetEvent;
import ar.com.score.monkey.task.event.SoundRecordedEvent;
import ar.com.score.monkey.ui.activity.GenerateSheetActivity;
import ar.com.score.monkey.ui.activity.HomeActivity;
import ar.com.score.monkey.ui.adapter.BaseRecyclerViewAdapter;
import ar.com.score.monkey.ui.adapter.RecordedSoundAdapter;
import ar.com.score.monkey.ui.adapter.SimpleItemTouchHelperCallback;
import ar.com.score.monkey.ui.view.LoadingLayout;
import ar.com.score.monkey.utils.ExtraConstants;
import ar.com.score.monkey.utils.ExtraConstants;
import ar.com.score.monkey.utils.FileManager;
import ar.com.score.monkey.utils.Lists;
import roboguice.inject.InjectView;

/**
 * Created by Emiliano on 28/04/2015.
 */
public class RecordedSoundFragment extends BaseFragment implements BaseRecyclerViewAdapter.OnEmptyList {
    public static final int GENERATE_SHEET_REQ =7;
    @Inject
    private Bus bus;
    @InjectView(R.id.recorded_sounds_list)
    private RecyclerView programationView;
    @InjectView(R.id.loading_layout)
    private LoadingLayout loadingLayout;
    @InjectView(R.id.emptyPlaceHolderList)
    private TextView emptyPlaceHolderList;
    private Long recordedNewSound;
    private List<RecordedSound> sounds;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.recorded_sound_fragment,container,false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        programationView.setHasFixedSize(true);
        programationView.setLayoutManager(new LinearLayoutManager(getActivity(), GridLayoutManager.VERTICAL, false));
        new GetRecordedSoundTask(getActivity(),((HomeActivity)getActivity()).getFilterString()).execute();
    }


    @Override
    public void onStart() {
        super.onStart();
        bus.getBus().registerSticky(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        bus.getBus().unregister(this);
    }

    public void onEvent(final OnGetRecordedSoundsEvent event){
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sounds = recordedNewSound != null && !Lists.isNullOrEmpty(event.items)  ? reorderList(event.items) : event.items;
                if (programationView.getAdapter() == null) {
                    RecordedSoundAdapter adapter = new RecordedSoundAdapter(getActivity(), sounds);
                    adapter.setOnItemClickListener(new BaseRecyclerViewAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClickListener(Object object, int position) {
                            Intent intent = new Intent(getActivity(), GenerateSheetActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(ExtraConstants.RECORDED_SOUND, (RecordedSound) object);
                            intent.putExtras(bundle);
                            getActivity().startActivityForResult(intent, GENERATE_SHEET_REQ);
                        }
                    });
                    ItemTouchHelper.Callback callback =
                            new SimpleItemTouchHelperCallback(adapter);
                    ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
                    touchHelper.attachToRecyclerView(programationView);
                    adapter.setOnEmptyList(RecordedSoundFragment.this);
                    programationView.setAdapter(adapter);

                } else {
                    ((RecordedSoundAdapter) programationView.getAdapter()).setNewItemList(sounds);
                }
                if (Lists.isNullOrEmpty(sounds)) {
                    emptyPlaceHolderList.setVisibility(View.VISIBLE);
                    programationView.setVisibility(View.GONE);

                } else {
                    emptyPlaceHolderList.setVisibility(View.GONE);
                    programationView.setVisibility(View.VISIBLE);


                }
                loadingLayout.stopLoading();
            }
        });

    }

    public List<RecordedSound> reorderList(List<RecordedSound> recordedSounds){
        RecordedSound recordedSoundToMove=null;

        for (RecordedSound recordedSound1:recordedSounds){
            if(recordedSound1.getId().equals(recordedNewSound)){
                recordedSoundToMove=recordedSound1;
                break;
            }
        }
        if(recordedSoundToMove!=null){
            recordedSounds.remove(recordedSoundToMove);

            List<RecordedSound> recordedSoundsToReturn= Lists.newArrayList(recordedSoundToMove);
            recordedSoundsToReturn.addAll(recordedSounds);
            return recordedSoundsToReturn;
        }
        return recordedSounds;
    }


    public void onEvent(SoundRecordedEvent event){
        recordedNewSound=event.id;
        loadingLayout.startLoading();
        new GetRecordedSoundTask(getActivity(),null).execute();
    }

    public void onEvent(OnFilterChange event){
        new GetRecordedSoundTask(getActivity(),event.string).execute();
    }

    @Override
    public void onEmptyList() {
        emptyPlaceHolderList.setVisibility(View.VISIBLE);
        programationView.setVisibility(View.GONE);
    }


}
