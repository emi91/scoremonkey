package ar.com.score.monkey.ui.activity;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;
import android.widget.Toast;

import ar.com.score.monkey.R;
import ar.com.score.monkey.ui.activity.commons.BaseActivityWithoutToolbar;
import ar.com.score.monkey.ui.fragment.BaseFragment;


public class SplashActivity extends BaseActivityWithoutToolbar implements Runnable{
//    public static int SPLASH_DELAY=3000;
    private Handler splashHandler;
    private MediaPlayer mediaPlayer;
    private boolean firstTime=true;
    @Override
    public Class<? extends BaseFragment> getFragmentClass() {
        return null;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.splash_activity;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImageView splash = (ImageView) this
                .findViewById(R.id.splash);
        splash.setBackgroundResource(R.drawable.splash);

        // Get the background, which has been compiled to an AnimationDrawable object.
        AnimationDrawable frameAnimation = (AnimationDrawable) splash.getBackground();
        frameAnimation.start();
        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.sound);
        mediaPlayer.setVolume(10,10);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                createSplashHandler();
            }
        });
        mediaPlayer.start();

    }

    private void createSplashHandler() {
        splashHandler = new Handler(Looper.getMainLooper());
        splashHandler.post(SplashActivity.this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(splashHandler!=null){
            splashHandler.removeCallbacks(this);
        }
        mediaPlayer.stop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(firstTime){
            firstTime=!firstTime;
        }else{
            createSplashHandler();
        }
    }

    @Override
    public void run() {
        startActivity(new Intent(SplashActivity.this,HomeActivity.class));
        SplashActivity.this.finish();
    }
}
