package ar.com.score.monkey.http;

/**
 * Created by Emiliano on 27/8/15.
 */
public class GenerateSheetResponse {
    private String midi;
    private String score;
    private String musicxml;

    public String getMidi() {
        return midi;
    }

    public void setMidi(String midi) {
        this.midi = midi;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getMusicxml() {
        return musicxml;
    }

    public void setMusicxml(String musicxml) {
        this.musicxml = musicxml;
    }
}
