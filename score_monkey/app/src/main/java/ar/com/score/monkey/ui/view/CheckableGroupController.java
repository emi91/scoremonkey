package ar.com.score.monkey.ui.view;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;

import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Emiliano on 23/5/15.
 */
public class CheckableGroupController {
    private List<WeakReference<View>> menuList=new ArrayList<WeakReference<View>>();
    private int selected=0;
    private OnSelectedListener onSelectListener;

    public CheckableGroupController(List<View> views) {
        for (View view:views){
            menuList.add(new WeakReference<View>(view));
        }
        if(menuList.get(selected).get()!=null){
            menuList.get(selected).get().setActivated(true);
            for (int i = 0; i < menuList.size(); i++) {
                final int finalI = i;
                menuList.get(i).get().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setSelected(finalI);
                    }
                });
            }
        }

    }

    public void setSelected(int finalI) {
        if(menuList.get(finalI).get()!=null){
            if(onSelectListener!=null)
                onSelectListener.onItemSelected(menuList.get(finalI).get(),finalI);
            for (int j = 0; j < menuList.size(); j++){
                menuList.get(j).get().setActivated(j == finalI);
                if(j==finalI)
                    selected=j;
            }
        }
    }

    public int getSelected() {
        return selected;
    }


    public interface OnSelectedListener{
        public void onItemSelected(View view,int position);
    }

    public void setOnSelectListener(OnSelectedListener onSelectListener) {
        this.onSelectListener = onSelectListener;
    }


}
