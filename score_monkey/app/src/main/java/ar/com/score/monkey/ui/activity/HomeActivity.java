package ar.com.score.monkey.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.inject.Inject;

import ar.com.score.monkey.R;
import ar.com.score.monkey.client.Bus;
import ar.com.score.monkey.task.event.ActivityResultEvent;
import ar.com.score.monkey.task.event.OnFilterChange;
import ar.com.score.monkey.ui.activity.commons.BaseActivityWithToolBar;
import ar.com.score.monkey.ui.activity.commons.BaseActivityWithoutToolbar;
import ar.com.score.monkey.ui.fragment.BaseFragment;
import ar.com.score.monkey.ui.fragment.HomeFragment;
import ar.com.score.monkey.ui.fragment.RecordFragment;
import de.greenrobot.event.EventBus;
import com.crittercism.app.Crittercism;


public class HomeActivity extends BaseActivityWithToolBar {
    @Inject
    public Bus bus;
    @Override
    public Class<? extends BaseFragment> getFragmentClass() {
        return HomeFragment.class;
    }
    private String filterString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Crittercism.initialize(getApplicationContext(), "5626f2bad224ac0a00ed40d5");
        ((EditText)((SearchView)getToolbar().findViewById(R.id.search)).findViewById(R.id.search_src_text)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                filterString=charSequence.toString();
                bus.getBus().post(new OnFilterChange(filterString));
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        getToolbar().findViewById(R.id.button).setVisibility(View.GONE);
        getToolbar().findViewById(R.id.share).setVisibility(View.GONE);
    }
    private void search(String s) {
        Toast.makeText(HomeActivity.this, "asd", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        bus.getBus().post(new BackEvent());

    }

    public EventBus getBus() {
        return bus.getBus();
    }

    public void findAudio() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("audio/*");

        startActivityForResult(Intent.createChooser(intent, "Pickea un audio"), RecordFragment.REQ_CODE_PICK_SOUNDFILE);
    }

    public static class BackEvent{

    }


    public boolean isRecordFragmentVisible(){
        HomeFragment homeFragment= (HomeFragment) getSupportFragmentManager().findFragmentByTag(HomeFragment.class.getName()
        );
        return homeFragment!=null && homeFragment.isRecordFragmentVisible();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        bus.getBus().postSticky(new ActivityResultEvent(resultCode, requestCode, data));
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.home_activity;
    }

    public void hideSearch(){
        (getToolbar().findViewById(R.id.search)).setVisibility(View.GONE);
    }
    public void showSearch(){
        (getToolbar().findViewById(R.id.search)).setVisibility(View.VISIBLE);
    }

    public String getFilterString() {
        return filterString;
    }


}
