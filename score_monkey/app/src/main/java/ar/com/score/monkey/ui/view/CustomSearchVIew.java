package ar.com.score.monkey.ui.view;

import android.content.Context;
import android.support.v7.widget.SearchView;
import android.util.AttributeSet;

/**
 * Created by Emiliano on 13/9/15.
 */
public class CustomSearchVIew extends SearchView {
    public CustomSearchVIew(Context context) {
        super(context);
    }

    public CustomSearchVIew(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSearchVIew(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
