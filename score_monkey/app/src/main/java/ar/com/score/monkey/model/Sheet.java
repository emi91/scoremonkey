package ar.com.score.monkey.model;

import java.io.Serializable;
import java.util.Date;

import ar.com.score.monkey.utils.DateUtils;

/**
 * Created by Emiliano on 3/8/15.
 */
public class Sheet implements Serializable {
    public Sheet() {
    }

    public Sheet(String name) {
        this.name = name;
    }

    private String name;
    private Long id;
    private String musicxmlUrl;
    private String scoreUrl;
    private String midiPath;
    private String date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMusicxmlUrl() {
        return musicxmlUrl;
    }

    public void setMusicxmlUrl(String musicxmlUrl) {
        this.musicxmlUrl = musicxmlUrl;
    }

    public String getScoreUrl() {
        return scoreUrl;
    }

    public void setScoreUrl(String scoreUrl) {
        this.scoreUrl = scoreUrl;
    }


    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMidiPath() {
        return midiPath;
    }

    public void setMidiPath(String midiPath) {
        this.midiPath = midiPath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sheet sheet = (Sheet) o;

        if (name != null ? !name.equals(sheet.name) : sheet.name != null) return false;
        if (id != null ? !id.equals(sheet.id) : sheet.id != null) return false;
        if (musicxmlUrl != null ? !musicxmlUrl.equals(sheet.musicxmlUrl) : sheet.musicxmlUrl != null)
            return false;
        if (scoreUrl != null ? !scoreUrl.equals(sheet.scoreUrl) : sheet.scoreUrl != null)
            return false;
        if (midiPath != null ? !midiPath.equals(sheet.midiPath) : sheet.midiPath != null)
            return false;
        return !(date != null ? !date.equals(sheet.date) : sheet.date != null);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (musicxmlUrl != null ? musicxmlUrl.hashCode() : 0);
        result = 31 * result + (scoreUrl != null ? scoreUrl.hashCode() : 0);
        result = 31 * result + (midiPath != null ? midiPath.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        return result;
    }
}
