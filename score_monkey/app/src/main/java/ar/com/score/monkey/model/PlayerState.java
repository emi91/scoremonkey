package ar.com.score.monkey.model;

/**
 * Created by emi91_000 on 08/08/2015.
 */
public enum PlayerState {
    PLAYING(true,false,false),
    EMPTY(false,false,false),
    NOT_EMPTY(false,true,true);

    private boolean stopEnabled;
    private boolean playEnabled;
    private boolean deleteEnabled;

    private PlayerState(boolean stopEnabled, boolean playEnabled, boolean deleteEnabled) {
        this.stopEnabled = stopEnabled;
        this.playEnabled = playEnabled;
        this.deleteEnabled = deleteEnabled;
    }

    public boolean isStopEnabled() {
        return stopEnabled;
    }

    public boolean isPlayEnabled() {
        return playEnabled;
    }

    public boolean isDeleteEnabled() {
        return deleteEnabled;
    }
}
